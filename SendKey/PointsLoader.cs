﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SendKey
{
    public class PointLoader
    {
        static bool _loaded = false;
        private static Dictionary<string, MouseHookHelper.POINT> _points;

        static PointLoader()
        {
            _points = new Dictionary<string, MouseHookHelper.POINT>();
        }
        private static void LoadPoint()
        {
            if (!_loaded)
            {
                _loaded = true;
                var configPath = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "Point.config");
                var points = File.ReadAllLines(configPath);
                System.Text.RegularExpressions.Regex regex =
                    new System.Text.RegularExpressions.Regex(@"^([\w]+)=([\d]+),([\d]+)$");
                foreach (var point in points)
                {
                    var match = regex.Match(point);
                    if (match.Groups.Count == 4 && !_points.ContainsKey(match.Groups[1].Value))
                    {
                        _points.Add(match.Groups[1].Value, new MouseHookHelper.POINT
                        {
                            X = int.Parse(match.Groups[2].Value),
                            Y = int.Parse(match.Groups[3].Value)
                        });
                    }
                }
            }
        }

        public static MouseHookHelper.POINT GetPoint(string key)
        {
            LoadPoint();
            MouseHookHelper.POINT point;
            if (!_points.TryGetValue(key, out point))
            {
                point = new MouseHookHelper.POINT();
            }
            return point;
        }

    }
}
