﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SendKey
{
    public class InputHelper
    {
        /// <summary>
        /// 键盘按键
        /// </summary>
        /// <param name="key"></param>
        /// <param name="waitSecond">等待秒</param>
        public static void EnterKey(string key, double waitSecond = 0)
        {
            SendKeys.SendWait(key);
            if (waitSecond > 0)
                System.Threading.Thread.Sleep((int)(1000 * waitSecond));
        }

        /// <summary>
        /// 鼠标左点击
        /// </summary>
        /// <param name="point"></param>
        /// <param name="waitSecond">等待秒</param>
        public static void ClickMouse(MouseHookHelper.POINT point, double waitSecond = 0)
        {
            //先移动鼠标到指定位置
            MouseHookHelper.SetCursorPos(point.X, point.Y);
            System.Threading.Thread.Sleep(500);
            //按下鼠标左键
            MouseHookHelper.mouse_event(MouseHookHelper.MOUSEEVENTF_LEFTDOWN,
                        point.X,
                        point.Y, 0, 0);

            System.Threading.Thread.Sleep(100);
            //松开鼠标左键
            MouseHookHelper.mouse_event(MouseHookHelper.MOUSEEVENTF_LEFTUP,
                        point.X,
                        point.Y, 0, 0);
            if (waitSecond > 0)
                System.Threading.Thread.Sleep((int)(1000 * waitSecond));
        }
    }
}
