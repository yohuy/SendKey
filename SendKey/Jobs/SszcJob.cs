﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SendKey.Jobs
{
    public class SszcJob : BaseJob
    {
        MouseHookHelper.POINT kuaisu;
        MouseHookHelper.POINT zhanchang;
        MouseHookHelper.POINT paidui;
        MouseHookHelper.POINT jinchang;
        MouseHookHelper.POINT lichang;

        MouseHookHelper.POINT denglu1;
        MouseHookHelper.POINT denglu2;
        MouseHookHelper.POINT denglu3;
        MouseHookHelper.POINT denglu4;
        MouseHookHelper.POINT denglu5;

        public SszcJob()
        {
            kuaisu = PointLoader.GetPoint("kuaisu");
            zhanchang = PointLoader.GetPoint("zhanchang");
            paidui = PointLoader.GetPoint("paidui");
            jinchang = PointLoader.GetPoint("jinchang");
            lichang = PointLoader.GetPoint("lichang");
            denglu1 = PointLoader.GetPoint("denglu1");
            denglu2 = PointLoader.GetPoint("denglu2");
            denglu3 = PointLoader.GetPoint("denglu3");
            denglu4 = PointLoader.GetPoint("denglu4");
            denglu5 = PointLoader.GetPoint("denglu5");
        }
        public override void Run()
        {
            Console.WriteLine("史诗战场任务已经开始");
            while (true)
            {
                // 登陆游戏
                Denglu();

                // 排战场
                Paidui();
            }

            /*
            更多举例:
            SendKeys.SendWait("^C");  //Ctrl+C 组合键
            SendKeys.SendWait("+C");  //Shift+C 组合键
            SendKeys.SendWait("%C");  //Alt+C 组合键
            SendKeys.SendWait("+(AX)");  //Shift+A+X 组合键
            SendKeys.SendWait("+AX");  //Shift+A 组合键,之后按X键
            SendKeys.SendWait("{left 5}");  //按←键 5次
            SendKeys.SendWait("{h 10}");   //按h键 10次
            SendKeys.Send("汉字");  //模拟输入"汉字"2个字
            */
        }
        private void Denglu()
        {
            for (int i = 0; i < 2; i++)
            {
                InputHelper.ClickMouse(denglu1, 0.5);
                InputHelper.ClickMouse(denglu2, 0.5);
                InputHelper.EnterKey("mima", 1);
                InputHelper.ClickMouse(denglu3, 10);
            }
            InputHelper.ClickMouse(denglu4, 0.5);
            InputHelper.ClickMouse(denglu5, 8);
        }

        private void Paidui()
        {
            for (int i = 0; i < 10; i++)
            {
                //DateTime begin = DateTime.Now;
                // 排本
                InputHelper.EnterKey("h", 1);
                InputHelper.ClickMouse(kuaisu, 0.5);
                InputHelper.ClickMouse(zhanchang, 0.5);
                InputHelper.ClickMouse(paidui, 0.5);
                InputHelper.EnterKey("h", 1);
                InputHelper.ClickMouse(kuaisu, 0.5);
                InputHelper.ClickMouse(zhanchang, 0.5);
                InputHelper.ClickMouse(paidui, 0.5);

                // 进场
                InputHelper.ClickMouse(jinchang, 8);
                for (var j = 0; j < 2; j++)
                {
                    InputHelper.EnterKey("678 ", 6);
                }

                // 离场
                InputHelper.ClickMouse(lichang, 5);
                //DateTime end = DateTime.Now;
                //Console.WriteLine((end - begin).TotalMinutes);
            }
        }
    }
}
