﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SendKey.Jobs
{
    public class AnjianJob : BaseJob
    {
        MouseHookHelper.POINT jian1;
        MouseHookHelper.POINT jian2;
        MouseHookHelper.POINT jian3;
        MouseHookHelper.POINT jian4;
        public AnjianJob()
        {
            jian1 = new MouseHookHelper.POINT
            {
                X = 878,
                Y =648
            };

            jian2 = new MouseHookHelper.POINT
            {
                X = 1030,
                Y =656
            };
            jian3 = new MouseHookHelper.POINT
            {
                X = 946,
                Y =724
            };
            jian4 = new MouseHookHelper.POINT
            {
                X = 956,
                Y =548
            };
        }
        public override void Run()
        {
            Console.WriteLine("按键任务已经开始");
            while (true)
            {
                // 登陆游戏
                Denglu();
                 
            }
            /*
            更多举例:
            SendKeys.SendWait("^C");  //Ctrl+C 组合键
            SendKeys.SendWait("+C");  //Shift+C 组合键
            SendKeys.SendWait("%C");  //Alt+C 组合键
            SendKeys.SendWait("+(AX)");  //Shift+A+X 组合键
            SendKeys.SendWait("+AX");  //Shift+A 组合键,之后按X键
            SendKeys.SendWait("{left 5}");  //按←键 5次
            SendKeys.SendWait("{h 10}");   //按h键 10次
            SendKeys.Send("汉字");  //模拟输入"汉字"2个字
            */
        }
        private void Denglu()
        {
            InputHelper.EnterKey("1", 1);
            InputHelper.EnterKey("2", 1);
            InputHelper.EnterKey("1", 1);
            InputHelper.ClickMouse(jian1, 0.5);
            InputHelper.EnterKey("3", 1);
            InputHelper.EnterKey("1", 1);
            InputHelper.ClickMouse(jian2, 0.5);
            InputHelper.EnterKey("4", 1);
            InputHelper.EnterKey("1", 1);
            InputHelper.ClickMouse(jian3, 0.5);
            InputHelper.ClickMouse(jian4, 0.5);
        }
    }
}
