﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SendKey.Jobs;

namespace SendKey
{
    class Program
    {

        static void Main(string[] args)
        {
            begin:
            Console.WriteLine("======================= 命令 =======================");
            Console.WriteLine("================= F1:获取鼠标位置      ==============");
            Console.WriteLine("================= F9:开始任务         ===============");

            BaseJob job = new AnjianJob();

            var keyInfo = Console.ReadKey();
            if (keyInfo.Key == ConsoleKey.F1)
            {
                MouseHookHelper.MOUSEPOINT mousePoint;
                if (MouseHookHelper.GetCursorPos(out mousePoint))
                {
                    Console.WriteLine(mousePoint);
                }
                goto begin;
            }
            else if (keyInfo.Key == ConsoleKey.F9)
            {
                Thread task = new Thread(() =>
                {
                    job.Run();
                });
                task.Start();
                Console.WriteLine("====================== F10停止 =====================");
                while (true)
                {
                    keyInfo = Console.ReadKey();
                    if (keyInfo.Key == ConsoleKey.F10)
                    {
                        Console.WriteLine("===================== 任务已停止 ====================");
                        task.Abort();
                        goto begin;
                    }
                }
            }
            else
            {
                goto begin;
            }
        }
    }
}
